package com.accenture.camel.routes;



import org.apache.camel.builder.RouteBuilder;

import com.accenture.camel.processors.GuardarMsjProcessor;

public class GuardarMsjRuta extends RouteBuilder {

	

	@Override
	public void configure() throws Exception {
		// TODO Auto-generated method stub
		from("direct:guardarmsj").process(new GuardarMsjProcessor()).to("seda:guardarmsj");
	}

}
