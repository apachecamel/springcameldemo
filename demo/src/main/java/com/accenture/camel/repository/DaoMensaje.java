package com.accenture.camel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.accenture.camel.model.Mensaje;

@Repository
public interface DaoMensaje extends JpaRepository<Mensaje, Long> {
	
}	
