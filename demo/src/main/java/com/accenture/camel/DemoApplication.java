package com.accenture.camel;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.accenture.camel.routes.BasicRoute;

@SpringBootApplication
public class DemoApplication {

//	private static final String CAMEL_URL_MAPPING = "/api/*";
//    private static final String CAMEL_SERVLET_NAME = "CamelServlet";
//	@Autowired
//	static
//	CamelContext ctxCamel;
	
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
//		try {
//			ctxCamel.addRoutes(new BasicRoute());
//			ctxCamel.startAllRoutes();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	}
	
//	@Bean
//    public ServletRegistrationBean servletRegistrationBean() {
//        ServletRegistrationBean registration =
//                new ServletRegistrationBean(new CamelHttpTransportServlet(), CAMEL_URL_MAPPING);
//        registration.setName(CAMEL_SERVLET_NAME);
//        return registration;
//    }

}
