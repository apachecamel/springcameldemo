package com.accenture.camel.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.accenture.camel.controllers.Controller;
import com.accenture.camel.model.Mensaje;
import com.accenture.camel.repository.DaoMensaje;

public class GuardarMsjProcessor implements Processor {
//	@Autowired
//	DaoMensaje daomsj;

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		Message s = exchange.getIn();
		JSONObject jin = (JSONObject) s.getBody();
		Mensaje msj = new Mensaje();
		
		
		msj.setMsj(jin.getString("msj"));
		msj.setToken(jin.getBoolean("token"));
		System.out.println(msj.getMsj()+" Aca estoy en procesor");
		
		Controller control = new Controller();
//		boolean error = ;
			
		if(!control.postMensaje(msj)) {
		jin.put("error", 0);
		s.setBody(jin, JSONObject.class);
		}else
		jin.put("error", 1);
		
		exchange.setOut(s);
	}

}
