package com.accenture.camel.controllers;

import org.apache.camel.CamelContext;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.ProducerTemplate;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.camel.model.Mensaje;
import com.accenture.camel.repository.DaoMensaje;
import com.accenture.camel.routes.BasicRoute;
import com.accenture.camel.routes.GuardarMsjRuta;

@RestController
public class Controller {

	@Autowired
	ProducerTemplate producer;
	@Autowired
	ConsumerTemplate consumer;
	@Autowired
	CamelContext ctxCamel;
	@Autowired
	DaoMensaje daomsj;
	
	
	@RequestMapping(value = "/")
	private void startCamel() throws Exception {
		ctxCamel.addRoutes(new BasicRoute());
		ctxCamel.startAllRoutes();
			
		
		producer.sendBody("direct:start","Calling via S.Boot");
		consumer.receiveBody("seda:end",String.class);
	}

	@PostMapping("/guardarmsj")
	private ResponseEntity<Object> guardarmsj(@RequestBody Mensaje msj) throws Exception {
		ctxCamel.addRoutes(new GuardarMsjRuta());
		ctxCamel.startAllRoutes();
		JSONObject enviado = new JSONObject();
		
		
		enviado.put("msj", msj.getMsj());
		enviado.put("token", msj.isToken());
		System.out.println(enviado.getString("msj"));
		producer.sendBody("direct:guardarmsj",enviado);
		
		JSONObject recibido = consumer.receiveBody("seda:guardarmsj",JSONObject.class);
		
		if(recibido.getInt("error")==0)
		return ResponseEntity.ok().body(recibido.toString());
		else
			return ResponseEntity.badRequest().body(recibido.toString());
	}
	
	public boolean postMensaje(Mensaje msj) {
		
		if(daomsj != null) {
			System.out.println("Post Mensaje True");
			daomsj.save(msj);
			return false;
		}else {
			System.out.println("PostMensaje False");
			return true;
	}}

}